package com.demo.cassandra;

public class Customer {
    public String firstName;
    public String lastName;
    public String accountId;
    public String city;
    public String postal;
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public String getAccountId() {
        return accountId;
    }
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getPostal() {
        return postal;
    }
    public void setPostal(String postal) {
        this.postal = postal;
    }

    public Customer(String firstName, String lastName, String accountId, String customerCity, String customerPostal) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountId = accountId;
        this.city = city;
        this.postal = postal;    }
    
}
