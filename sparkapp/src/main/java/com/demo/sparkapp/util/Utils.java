package com.demo.sparkapp.util;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;

public class Utils {
    public static boolean checkIfFileIsPresent(String path) throws IOException {
        Path pt = new Path(path);
        FileSystem fs = FileSystem.get(new Configuration());
        return fs.isFile(pt);
    }
}
