# ETL Design

A quick coding challenge for ETL. It consists of three parts. If you want to use it, go and have fun. No need to install anything. It uses containers. Figure below gives the overall picture


![](image.png)
Figure 1: Overall architecture 

## Infrastructure : 

For the infrastructure we use docker compose to set my application services which contains :
   - Spark ,
   -  Hdfs , 
   - Cassandra DB .

To run the Docker container : docker-compose up
Used pre-exiting containers in docker compose. I have been using this for my personal project.

## Load Layer : 

I built a script that will be able to get my inputs and load it to hdfs folder using the ( hdfs dfs –put command )

## Transform Layer : 

Spark is used to:
To run it, build the project using maven
- join the tables customer and address by the key Address ID to make customer_joined
- join the tables address and transaction by the key Address ID  to make transaction_joined 
- finally joined the tables customer_joined and transaction_joined by the key Account ID 

And then we store the joined tables in Cassandra DB which we can query using CassandraSQL.

Built also a restful app that can query Cassandra db table and load data as a json response.(To run it, build the project using maven)