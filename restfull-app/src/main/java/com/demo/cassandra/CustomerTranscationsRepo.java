package com.demo.cassandra;
import java.util.List;

import org.springframework.data.cassandra.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface CustomerTranscationsRepo extends CrudRepository<CustomerTranscations,CustomerTranscationsKey> {
    @Query("SELECT * FROM customer_transaction_city WHERE  city = ?0 and income > ?1  and income < ?2 and transaction_month = ?3 and transaction_amount > ?4 ALLOW FILTERING")
    List<CustomerTranscations> findByIncomeAndCity(String city, double incomeLower,double incomeUpper, int transaction_month, double transaction_amount);
}