package com.demo.cassandra;

import static org.springframework.data.cassandra.core.cql.PrimaryKeyType.PARTITIONED;

import org.springframework.data.cassandra.core.mapping.PrimaryKeyClass;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

@PrimaryKeyClass
public class CustomerTranscationsKey {
    
    @PrimaryKeyColumn("city")
    private String transcationCity;
    @PrimaryKeyColumn("transaction_amount")
    private double transcationAmount;
    @PrimaryKeyColumn
    private double income;
    @PrimaryKeyColumn(name ="account_id", type = PARTITIONED)
    private String accountId;
    @PrimaryKeyColumn(name ="transaction_month")
    private int transcationMonth;
    
    
    
    public CustomerTranscationsKey(String transcationCity, double transcationAmount, double income, String accountId, int transcationMonth) {
        super();
        this.transcationCity = transcationCity;
        this.transcationAmount = transcationAmount;
        this.income = income;
        this.accountId = accountId;
        this.transcationMonth = transcationMonth;
    }
    public String getTranscationCity() {
        return transcationCity;
    }
    public void setTranscationCity(String transcationCity) {
        this.transcationCity = transcationCity;
    }
   
    public double getTranscationAmount() {
        return transcationAmount;
    }
    public void setTranscationAmount(double transcationAmount) {
        this.transcationAmount = transcationAmount;
    }
   
    public String getAccountId() {
        return accountId;
    }
    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }
    public double getIncome() {
        return income;
    }
    public void setIncome(double income) {
        this.income = income;
    }
    public int getTranscationMonth() {
        return transcationMonth;
    }
    public void setTranscationMonth(int transcationMonth) {
        this.transcationMonth = transcationMonth;
    }
    
    
}
