package com.demo.cassandra;

import java.time.LocalDateTime;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;

@Table("customer_transaction_city")
public class CustomerTranscations {

    @PrimaryKey
    private CustomerTranscationsKey key;

    @Column("customer_last_name")
    private String lastName;
    @Column("customer_first_name")
    private String firstName;
    @Column("customer_city")
    private String customerCity;
    @Column("customer_postal")
    private String customerPostal;
    @Column("postal")
    private String transcationPostal;
    @Column("transaction_date")
    private com.datastax.driver.core.LocalDate transactionDate;
    @Column("transaction_day")
    private int transactionDay;
    @Column("transaction_year")
    private int tranactionYear;

    public CustomerTranscations(CustomerTranscationsKey key, String lastName, String firstName, String customerCity, String customerPostal, String transcationPostal, com.datastax.driver.core.LocalDate transactionDate, int transactionDay, int tranactionYear) {
        super();
        this.key = key;
        this.lastName = lastName;
        this.firstName = firstName;
        this.customerCity = customerCity;
        this.customerPostal = customerPostal;
        this.transcationPostal = transcationPostal;
        this.transactionDate = transactionDate;
        this.transactionDay = transactionDay;
        this.tranactionYear = tranactionYear;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getCustomerCity() {
        return customerCity;
    }

    public void setCustomerCity(String customerCity) {
        this.customerCity = customerCity;
    }

    public String getCustomerPostal() {
        return customerPostal;
    }

    public void setCustomerPostal(String customerPostal) {
        this.customerPostal = customerPostal;
    }

    public String getTranscationPostal() {
        return transcationPostal;
    }

    public void setTranscationPostal(String transcationPostal) {
        this.transcationPostal = transcationPostal;
    }

    public com.datastax.driver.core.LocalDate getTransactionDate() {
        return transactionDate;
    }

    public void setTransactionDate(com.datastax.driver.core.LocalDate transactionDate) {
        this.transactionDate = transactionDate;
    }

    public int getTransactionDay() {
        return transactionDay;
    }

    public void setTransactionDay(int transactionDay) {
        this.transactionDay = transactionDay;
    }

    public int getTranactionYear() {
        return tranactionYear;
    }

    public void setTranactionYear(int tranactionYear) {
        this.tranactionYear = tranactionYear;
    }

    public CustomerTranscationsKey getKey() {
        return key;
    }

    public void setKey(CustomerTranscationsKey key) {
        this.key = key;
    }

}
