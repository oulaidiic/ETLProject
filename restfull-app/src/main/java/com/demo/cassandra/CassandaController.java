package com.demo.cassandra;

import java.time.Month;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@RestController
public class CassandaController {
    @Autowired 
    private CustomerTranscationsRepo repo;
    ObjectMapper mapper = new ObjectMapper();

    @RequestMapping("/query")
    public String query(@RequestParam("income") String incomerange, @RequestParam("city") String city, @RequestParam("month") String month, @RequestParam("expense") String spentAmount) throws JsonProcessingException {
        String[] income = incomerange.split("-");
        Double incomeLower = Double.parseDouble(income[0]);
        Double incomeUpper = Double.parseDouble(income[1]);
        List<CustomerTranscations> result = repo.findByIncomeAndCity(city, incomeLower, incomeUpper, Month.valueOf(month.toUpperCase()).getValue(), Double.parseDouble(spentAmount));
        Set<Customer> customer = new HashSet<Customer>();
        for (CustomerTranscations r : result) {
            customer.add(new Customer(r.getFirstName(), r.getLastName(), r.getKey().getAccountId(), r.getCustomerCity(), r.getCustomerPostal()));
        }
        String jsonInString = mapper.writeValueAsString(customer);
        return jsonInString;
    }
    @RequestMapping("/test")
    public String test() {
        return "Web server is reachable";
    }

}
