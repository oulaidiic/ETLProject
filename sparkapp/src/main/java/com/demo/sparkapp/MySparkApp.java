package com.demo.sparkapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import com.demo.sparkapp.util.*;
import org.apache.spark.sql.cassandra.*;


public class MySparkApp {

    public static void main(String[] args) throws Exception {
        MySparkApp app = new MySparkApp();
        app.run(args);
    }

    public void run(String[] args) throws Exception {
        /**
         * Probably we need more test to check the proper file is picked for
         * this demo I am settling on checking the files are present
         */
        if (!Utils.checkIfFileIsPresent(args[0]) && !Utils.checkIfFileIsPresent(args[1]) && !Utils.checkIfFileIsPresent(args[2])) {
            System.err.println("The files to be joined are not present");
            System.exit(-1);
        }
        SparkSession session = SparkSession.builder().appName("MySparkApp").config("spark.cassandra.connection.host", "cassandra").getOrCreate();

        Dataset<org.apache.spark.sql.Row> customers = session.read().option("header", true).csv(args[0]);
        Dataset<org.apache.spark.sql.Row> address = session.read().option("header", true).csv(args[1]);
        Dataset<org.apache.spark.sql.Row> transcations = session.read().option("header", true).csv(args[2]);
        ArrayList<String> joinColList = new ArrayList<String>();
        joinColList.add("address_id");
        Dataset<org.apache.spark.sql.Row> customerAddressJoined = customers.join(address, scala.collection.JavaConversions.asScalaBuffer(joinColList), "inner").toDF("address_id", "customer_first_name", "customer_last_name", "account_id", "income", "customer_city", "customer_postal");
        customerAddressJoined = customerAddressJoined.select("customer_first_name", "customer_last_name", "account_id", "income", "customer_city", "customer_postal");
        Map<String, String> map = new HashMap<String, String>();
        map.put("cluster", "demo");
        map.put("keyspace", "example");
        map.put("table", "customer_transaction");
        // customerAddressJoined.write().format("org.apache.spark.sql.cassandra").options(map).mode(SaveMode.Overwrite).save();
        customerAddressJoined.show();
        ArrayList<String> joinColList2 = new ArrayList<String>();
        joinColList2.add("account_id");
        Dataset<org.apache.spark.sql.Row> transcationAddressJoined = transcations.join(address, scala.collection.JavaConversions.asScalaBuffer(joinColList), "inner");
        transcationAddressJoined= transcationAddressJoined.selectExpr("account_id","transaction_amount","transaction_date", "split(transaction_date, '-')[0] as transaction_year","split(transaction_date, '-')[1] as transaction_month", "split(transaction_date, '-')[2] as transaction_day","city","postal");

        transcationAddressJoined.show();
        Dataset<org.apache.spark.sql.Row> overalJoined = transcationAddressJoined.join(customerAddressJoined, scala.collection.JavaConversions.asScalaBuffer(joinColList2), "inner");
        overalJoined.show();
       overalJoined.write().format("org.apache.spark.sql.cassandra").options(map).mode(SaveMode.Overwrite).save();

    }

}
