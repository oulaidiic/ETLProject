#/bin/bash
cc=`docker ps | grep namenode | awk '{print $1}'`
docker cp loadfiles.sh $cc:/opt/
docker cp mydata $cc:/opt/
docker exec $cc sh -c 'chmod +x /opt/loadfiles.sh'
docker exec $cc sh -c '/opt/loadfiles.sh'
